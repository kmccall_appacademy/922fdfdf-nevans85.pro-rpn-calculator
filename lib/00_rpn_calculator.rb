require 'byebug'

class RPNCalculator
  attr_accessor :stack

  def initialize
    @stack = []
  end

  def value
    @stack.last
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def evaluate(string)
    tokened_string = tokens(string)
    tokened_string.each do |token|
      case token
      when :+
        plus
      when :-
        minus
      when :*
        times
      when :/
        divide
      else
        push(token)
      end
    end
    value
  end

  def tokens(string)
    digits = %w[1 2 3 4 5 6 7 8 9 0]
    tokened_array = []
    string.split.each do |e|
      tokened_array << :+ if e == '+'
      tokened_array << :- if e == '-'
      tokened_array << :* if e == '*'
      tokened_array << :/ if e == '/'
      tokened_array << e.to_i if digits.include?(e)
    end
    tokened_array
  end

  private

  def perform_operation(op_symbol)
    raise 'calculator is empty' if @stack.length < 2
    num2 = @stack.pop
    num1 = @stack.pop
    case op_symbol
    when :+
      @stack << num1 + num2
    when :-
      @stack << num1 - num2
    when :*
      @stack << num1 * num2
    when :/
      @stack << num1.to_f / num2
    end
  end
end
